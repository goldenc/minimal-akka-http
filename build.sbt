name := "minimal-akka-http"

version := "0.1"

scalaVersion := "2.12.4"

// For Akka 2.4.x or 2.5.x
libraryDependencies += "com.typesafe.akka" %% "akka-http" % "10.0.11"
// Only when running against Akka 2.5 explicitly depend on akka-streams in same version as akka-actor
libraryDependencies += "com.typesafe.akka" %% "akka-stream" % "2.5.7" // or whatever the latest version is
libraryDependencies += "com.typesafe.akka" %% "akka-actor"  % "2.5.7" // or whatever the latest version is

// JSON
libraryDependencies += "com.typesafe.akka" %% "akka-http-spray-json" % "10.0.11"

// Swagger
libraryDependencies += "com.github.swagger-akka-http" %% "swagger-akka-http" % "0.11.0"

// Tests
libraryDependencies += "com.typesafe.akka" %% "akka-http-testkit" % "10.0.11"
libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.4"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.4" % "test"