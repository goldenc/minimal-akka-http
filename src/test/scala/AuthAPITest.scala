import org.scalatest.{Matchers, WordSpec}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import server.AuthRoutes


class AuthAPITest extends WordSpec with Matchers with ScalatestRouteTest {

  "The auth service" should {

    "return an access token when provided with correct username and password" in {
      Post(s"/auth/${10}/token?userName=fred&password=fredspassword") ~> AuthRoutes.routes ~> check {
        responseAs[String].contains("fakeaccesstoken") shouldBe true
      }
    }
  }

}
