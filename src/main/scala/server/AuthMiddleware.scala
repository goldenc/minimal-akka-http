package server

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Directive1

object AuthMiddleware {

  def getAccessToken: Directive1[String] = headerValueByName("accessToken")

}
