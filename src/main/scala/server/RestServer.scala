package server

import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._

import scala.io.StdIn

object RestServer extends WebService {

  def main(args: Array[String]) {

    val allRoutes = path("/") {
      AuthRoutes.routes
    }

    val bindingFuture = Http().bindAndHandle(AuthRoutes.routes, "localhost", 8080)

    println(s"Server online at http://localhost:8080/\nPress RETURN to stop...")
    StdIn.readLine()
    bindingFuture
      .flatMap(_.unbind())
      .onComplete(_ => system.terminate())
  }
}
