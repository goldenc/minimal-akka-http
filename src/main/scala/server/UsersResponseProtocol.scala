package server

import spray.json.{DefaultJsonProtocol, JsValue, RootJsonFormat}

object UsersResponseProtocol extends DefaultJsonProtocol {

  case class UserInfoRequest(userName: String, accessToken: String)

  sealed trait UsersResponse
  case class UserInfo(favouriteColor: String) extends UsersResponse
  case class Unauthorized() extends UsersResponse

  implicit val userInfoFormat = jsonFormat1(UserInfo)
  implicit val unauthorizedFormat = jsonFormat0(Unauthorized)

  implicit object UsersResponseFormat extends RootJsonFormat[UsersResponse] {
    override def read(json: JsValue): UsersResponse = ???

    override def write(obj: UsersResponse): JsValue = obj match {
      case userInfo @ UserInfo(_) => userInfo.toJson
      case failure @ Unauthorized() => failure.toJson
    }
  }

}
