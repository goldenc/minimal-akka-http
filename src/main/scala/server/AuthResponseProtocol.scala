package server

import spray.json.{DefaultJsonProtocol, JsValue, RootJsonFormat}

object AuthResponseProtocol extends DefaultJsonProtocol {

  case class AuthenticationRequest(userName: String, password: String)

  sealed trait AuthResponse
  case class TokenResponse(accessToken: String) extends AuthResponse
  case class AuthenticationFailure(reason: String) extends AuthResponse

  implicit val tokenFormat = jsonFormat1(TokenResponse)
  implicit val authenticationFailureFormat = jsonFormat1(AuthenticationFailure)

  implicit object AuthResponseJsonFormat extends RootJsonFormat[AuthResponse] {
    override def read(json: JsValue): AuthResponse = ???

    override def write(obj: AuthResponse): JsValue = obj match {
      case token @ TokenResponse(_) => token.toJson
      case failure @ AuthenticationFailure(_) => failure.toJson
    }
  }



}
