package server

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import server.AuthResponseProtocol.AuthenticationRequest
import server.UsersResponseProtocol.UserInfoRequest

object AuthRoutes {

  val routes : Route =
    path("auth" / IntNumber / "token") {
      num =>
        println(s"num was $num")
        parameters("userName", "password") {
          (userName, password) =>
          post {
              AuthService.getToken(AuthenticationRequest(userName, password))
          }
        }

    } ~
    path("userinfo") {
      parameters("userName") { userName =>
        AuthMiddleware.getAccessToken { accessToken =>
          get {
            AuthService.getUserInfo(UserInfoRequest(userName, accessToken))
          }
        }
      }

    }

}
