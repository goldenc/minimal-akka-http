package server

import akka.actor.{Actor, ActorLogging}
import server.AuthResponseProtocol.{AuthResponse, AuthenticationFailure, AuthenticationRequest, TokenResponse}
import server.UsersResponseProtocol.{Unauthorized, UserInfo, UserInfoRequest, UsersResponse}

class AuthActor extends Actor with ActorLogging {

  val users : Map[String, String] = Map("fred" -> "fredspassword")

  val theOneTrueToken : String = "fakeaccesstoken"

  def fakeHash(password: String) : String = password

  def authenticate(authenticationRequest: AuthenticationRequest) : AuthResponse = authenticationRequest match {
    case AuthenticationRequest(userName, password) =>
      if (users.contains(userName)) {
        val hashedPassword = users(userName)
        if (fakeHash(password) == hashedPassword) {
          TokenResponse(theOneTrueToken)
        } else {
          AuthenticationFailure("invalid password")
        }
      } else {
        AuthenticationFailure("unknown user")
      }
  }

  def getUserInfo(userInfoRequest: UserInfoRequest) : UsersResponse = userInfoRequest match {
    case UserInfoRequest(_, accessToken) => {
      if (accessToken == theOneTrueToken) {
        UserInfo("red")
      } else {
        Unauthorized()
      }
    }
  }

  override def receive: Receive = {
    case authenticationRequest @ AuthenticationRequest(_, _) => sender() ! authenticate(authenticationRequest)

    case userInfoRequest @ UserInfoRequest(_, _) => sender() ! getUserInfo(userInfoRequest)
  }
}