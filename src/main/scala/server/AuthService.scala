package server

import akka.actor.{ActorRef, Props}
import akka.http.scaladsl.server.StandardRoute
import akka.pattern.ask
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import server.AuthResponseProtocol.{AuthResponse, AuthenticationRequest}
import server.UsersResponseProtocol.{UserInfoRequest, UsersResponse}
import akka.util.Timeout
import scala.concurrent.duration._

object AuthService extends WebService {

  implicit val timeout: Timeout = 5.seconds

  val auth : ActorRef = system.actorOf(Props[AuthActor], "auth")

  def getToken(authenticationRequest: AuthenticationRequest) : StandardRoute =
    complete((auth ? authenticationRequest).mapTo[AuthResponse])

  def getUserInfo(userInfoRequest: UserInfoRequest) : StandardRoute =
    complete((auth ? userInfoRequest).mapTo[UsersResponse])


}
